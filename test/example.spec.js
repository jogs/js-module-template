describe("Ejemplo de pruebas unitarias", () => {
  it("1 debe ser de tipo numerico", () => {
    expect(1).to.be.a("number");
  });
  it('"uno" debe ser de tipo string', () => {
    expect("uno").to.be.a("string");
  });
  it("1 * uno debe ser NaN", () => {
    expect(1 * "uno").to.be.NaN;
  });
  it("5 * 9 debe ser 45", () => {
    expect(5 * 9).to.be.equal(45);
  });
});
